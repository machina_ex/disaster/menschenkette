#include <Arduino.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

const char* ssid     = "YourWiFiSSID";
const char* password = "yourWiFiPassword";

IPAddress local_ip(192, 168, 178, 115);
IPAddress gateway(192, 168, 178, 1);
IPAddress subnet(255, 255, 255, 0);

void setup()
{
    Serial.begin(115200);
    pinMode(36, INPUT);
    delay(10);
    Serial.print("Connecting to ");
    Serial.println(ssid);

    WiFi.config(local_ip, gateway, subnet);
    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected.");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}

boolean peopleConnected = false;
boolean peopleConnectedOld = true;
long connected_millis = millis();


void loop(){
  if(!digitalRead(36)){
    connected_millis = millis();
    peopleConnected = false;
  }
  if(connected_millis+500 == millis() || connected_millis > millis()){
    peopleConnected = true;
  }

 if(peopleConnected!=peopleConnectedOld){
   peopleConnectedOld=peopleConnected;
   Serial.println(peopleConnected);

   HTTPClient http;    //Declare object of class HTTPClient
    http.begin("192.168.178.51",8081, "/Tutorial/devices/http/Menschenkette");      //Specify request destination // plaiframe: port 8080 path /peopleconnection

    StaticJsonBuffer<300> JSONbuffer;   //Declaring static JSON buffer
    JsonObject& JSONencoder = JSONbuffer.createObject();

    delay(10);
    JSONencoder["connected"] = peopleConnected;

    char JSONmessageBuffer[300] = {0};
    JSONencoder.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));

    http.addHeader("Content-Type", "application/json");  //Specify content-type header

    Serial.print(JSONmessageBuffer) ;

    int httpCode = http.POST(JSONmessageBuffer);//Send the request
    Serial.println(httpCode);

    http.end();  //Close connection

 }

}
